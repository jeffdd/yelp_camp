const Campground = require('../models/campground');
const Comment = require('../models/comment');

module.exports = {
  isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
      return next();
    }
    req.flash('error', 'You need to be logged in to do that!');
    res.redirect('/login');
  },

  checkCampgroundOwnership(req, res, next) {
    // Is user logged in?
    if (req.isAuthenticated()) {
      Campground.findById(req.params.id, (err, foundCampground) => {
        if (err) {
          req.flash('error', 'Campground not found!');
          res.redirect('back');
        } else if (foundCampground && foundCampground.author.id.equals(req.user._id)) {
          next();
        } else {
          req.flash('error', 'You don\'t have permission to do that!');
          res.redirect('back');
        }
      });
    } else {
      req.flash('error', 'You need to be logged in to do that!');
      res.redirect('back');
    }
  },

  checkCommentOwnership(req, res, next) {
    // Is user logged in?
    if (req.isAuthenticated()) {
      Comment.findById(req.params.id, (err, foundComment) => {
        if (err) {
          req.flash('error', 'Comment not found!');
          res.redirect('back');
          //Does user own comment?
        } else if (foundComment && foundComment.author.id.equals(req.user._id)) {
          next();
        } else {
          req.flash('error', 'You don\'t have permission to do that!');
          res.redirect('back');
        }
      });
    } else {
      req.flash('error', 'You need to be logged in to do that!');
      res.redirect('back');
    }
  }
}