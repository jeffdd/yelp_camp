// -----------------------------------------------------------------------------
// CAMPGROUNDS ROUTES
// -----------------------------------------------------------------------------
const express = require('express'),
  Campground = require('../models/campground'),
  middleware = require('../middleware');


const router = express.Router({ mergeParams: true });

//INDEX - show all campgrounds
router.get('/', function (req, res) {
  // Get all campgrounds from db
  Campground.find({}, (err, allCampgrounds) => {
    if (err) {
      req.flash('error', 'Something went wrong :(');
      console.log(err);
    } else {
      res.render('campgrounds/index',
        { campgrounds: allCampgrounds, currentUser: req.user });
    }
  })
});

//CREATE - add new campground to database
router.post('/', middleware.isLoggedIn, function (req, res) {
  let newCampground = {
    name: req.body.name,
    price: req.body.price,
    image: req.body.image,
    description: req.body.description,
    author: {
      id: req.user._id,
      username: req.user.username
    }
  };

  //NEW - show form to create new campground
  Campground.create(newCampground, (err, newlyCreated) => {
    if (err) {
      req.flash('error', 'Something went wrong :(');
      console.log(err);
    } else {
      req.flash('Success', 'Succesfully created campground!');
      res.redirect('/campgrounds');
    }
  });
});

// EDIT campgrourd route
router.get('/:id/edit', middleware.checkCampgroundOwnership, (req, res) => {
  // Is user logged in?
  Campground.findById(req.params.id, (err, foundCampground) => {
    res.render('campgrounds/edit', { campground: foundCampground });
  });
})

// UPDATE campground route
router.put('/:id', middleware.checkCampgroundOwnership, (req, res) => {
  //Find and update correct campground
  Campground.findByIdAndUpdate(req.params.id, req.body.campground, (err, updatedCampground) => {
    if (err) {
      req.flash('error', 'Something went wrong :(');
      res.redirect('/campgrounds');
    } else {
      //Redirect to show page
      res.redirect('/campgrounds/' + req.params.id);
    }
  })
})

// DESTROY campground route
router.delete('/:id', middleware.checkCampgroundOwnership, (req, res) => {
  Campground.findByIdAndRemove(req.params.id, (err) => {
    if (err) {
      req.flash('error', 'Something went wrong :(');
      res.redirect('/campgrounds')
    } else {
      req.flash('Success', 'Succesfully deleted campground!');
      res.redirect('/campgrounds');
    }
  });
})

// Create new campground and add to db
router.get('/new', middleware.isLoggedIn, function (req, res) {
  res.render('campgrounds/new');
});

// SHOW
router.get('/:id', function (req, res) {
  // Find the campground by the provided id
  Campground.findById(req.params.id).populate('comments').exec((err, foundCampground) => {
    if (err) {
      console.log(err);
    } else {
      res.render('campgrounds/show', { campground: foundCampground });
    }
  });
});

// Export the campgrounds routes
module.exports = router;