// ----------------------------------------------------------------------------
//  INDEX ROUTES
// ----------------------------------------------------------------------------
const express = require('express'),
  passport = require('passport'),
  User = require('../models/user');

const router = express.Router();

router.get('/', function (req, res) {
  res.render('landing');
});


// ----------------------------------------------------------------------------
// AUTH Routes
// ----------------------------------------------------------------------------
// show register form
router.get('/register', (req, res) => {
  res.render('auth/register');
})

// handle sign up logic
router.post('/register', (req, res) => {
  let newUser = new User({ username: req.body.username });
  User.register(newUser, req.body.password, (err, user) => {
    if (err) {
      req.flash('error', err.message);
      res.redirect('/register')
    }
    passport.authenticate('local')(req, res, () => {
      req.flash('success', 'Welcome to YelpCamp ' + user.username);
      res.redirect('/campgrounds')
    });
  });
})

// LOGIN ROUTE
// show login form
router.get('/login', (req, res) => {
  res.render('auth/login');
})

router.post('/login', passport.authenticate('local',
  { successRedirect: '/campgrounds', failureRedirect: '/login' }),
  (req, res) => { })


// LOGOUT ROUTE
router.get('/logout', (req, res) => {
  req.logout()
  req.flash('success', 'Logged you out!');
  res.redirect('/campgrounds');
})

module.exports = router;