// ============================================================================
// Commments Routes
// ============================================================================
const express = require('express'),
  Campground = require('../models/campground'),
  Comment = require('../models/comment'),
  middleware = require('../middleware');

const router = express.Router({ mergeParams: true });

// NEW ROUTE
router.get('/new', middleware.isLoggedIn, function (req, res) {
  // Find campground by id
  Campground.findById(req.params.id, (err, campground) => {
    if (err) {
      req.flash('error', 'Something went wrong :(');
      res.redirect('/campgrounds')
    } else {
      res.render('comments/new', { campground: campground })
    }
  })
});

// CREATE ROUTE
router.post('/', middleware.isLoggedIn, function (req, res) {
  //Lookup campground by id
  Campground.findById(req.params.id, (err, campground) => {
    if (err) {
      req.flash('error', 'Something went wrong :(');
      res.redirect('/campgrounds');
    } else {
      // create new comment
      Comment.create(req.body.comment, (err, comment) => {
        if (err) {
          req.flash('error', 'Error creating comment');
          console.log("Error creating new comment");
        } else {
          // add username and id to new comment
          comment.author.id = req.user._id;
          comment.author.username = req.user.username;
          comment.save();

          //connect new comment to campground
          campground.comments.push(comment);
          campground.save();
          //redirect campground show page
          req.flash('Success', 'Succesfully added comment!');
          res.redirect('/campgrounds/' + campground._id);
        }
      });
    }
  })
});

// EDIT ROUTE
router.get('/:comment_id/edit', middleware.checkCommentOwnership, (req, res) => {
  Comment.findById(req.params.comment_id, (err, foundComment) => {
    if (err) {
      req.flash('error', 'Something went wrong :(');
      res.redirect('back');
    } else {
      res.render('comments/edit', {
        campground_id: req.params.id,
        comment: foundComment
      });
    }
  });
})

// UPDATE ROUTE
router.put('/:comment_id', middleware.checkCommentOwnership, (req, res) => {
  Comment.findByIdAndUpdate(req.params.comment_id, req.body.comment, (err, updatedComment) => {
    if (err) {
      req.flash('error', 'Something went wrong :(');
      res.redirect('back');
    } else {
      res.redirect('/campgrounds/' + req.params.id);
    }
  });
})

// DESTROY Route
router.delete('/:comment_id', middleware.checkCommentOwnership, (req, res) => {
  Comment.findByIdAndRemove(req.params.comment_id, (err) => {
    if (err) {
      req.flash('error', 'Something went wrong :(');
      res.redirect('back');
    } else {
      req.flash('Success', 'Succesfully deleted comment!');
      res.redirect('/campgrounds/' + req.params.id);
    }
  })
})

module.exports = router;